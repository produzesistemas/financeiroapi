﻿using System.ComponentModel.DataAnnotations.Schema;

namespace Model
{
    [Table("AssinaturaPlano")]
    public class AssinaturaPlano : BaseEntity
    {
        public string Descricao { get; set; }
        public bool Ativo { get; set; }
        public int Meses { get; set; }
    }
}
