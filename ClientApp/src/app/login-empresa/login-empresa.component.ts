import { Component, OnInit, Renderer2 } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { Router } from '@angular/router';
import { AuthenticationService } from '../_services/authentication.service';
import { AuthenticationSocialLoginService } from '../_services/authentication-social-login.service';
import { ApplicationUser } from '../_model/application-user';
import { SocialAuthService } from 'angularx-social-login';
import { FacebookLoginProvider, GoogleLoginProvider } from 'angularx-social-login';
import { LoginUser } from '../_model/login-user-model';
import { ToastrService } from 'ngx-toastr';

@Component({
    selector: 'app-login-empresa',
    templateUrl: './login-empresa.component.html'
})

export class LoginEmpresaComponent implements OnInit {
    form: FormGroup;
    public submitted = false;
    public loginUser: LoginUser = new LoginUser();
    constructor(
        private router: Router,
        private authenticationService: AuthenticationService,
        private toastr: ToastrService,
        private formBuilder: FormBuilder,) {
    }

    ngOnInit() {
        if (this.authenticationService.getCurrentUser()) {
            return this.router.navigate(['/dashboard']);
        }
        
      
        this.form = this.formBuilder.group({
            email: ['', Validators.required],
            secret: ['', Validators.required]
        });
        
    }

    get f() { return this.form.controls; }


    onLogin() {
        this.submitted = true;
        if (this.form.invalid) {
            return;
        }
        this.loginUser.email = this.form.controls.email.value;
        this.loginUser.secret = this.form.controls.secret.value;
        this.authenticationService.loginEmpresa(this.loginUser)
        .subscribe(result => {
            this.authenticationService.clearUser();
            this.authenticationService.addCurrenUser(result);
            return this.router.navigate(['/dashboard']);
        });
    }



}

