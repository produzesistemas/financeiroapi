﻿using System.ComponentModel.DataAnnotations.Schema;

namespace Model
{
    [Table("AssinaturaFormaPagamento")]
    public class AssinaturaFormaPagamento : BaseEntity
    {
        public string Descricao { get; set; }
    }
}
