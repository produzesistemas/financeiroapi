﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace Model
{
    [Table("AssinaturaEmpresa")]
    public class AssinaturaEmpresa : BaseEntity
    {
        public string ApplicationUserId { get; set; }
        public int EmpresaId { get; set; }
        public int AssinaturaPlanoId { get; set; }
        public int AssinaturaFormaPagamentoId { get; set; }
        public DateTime CreateDate { get; set; }
        public DateTime ExpirationDate { get; set; }
        public decimal ValorPago { get; set; }

        [NotMapped]
        public ApplicationUser ApplicationUser { get; set; }
        [NotMapped]
        public AssinaturaPlano AssinaturaPlano { get; set; }
        [NotMapped]
        public AssinaturaFormaPagamento AssinaturaFormaPagamento { get; set; }
    }
}
